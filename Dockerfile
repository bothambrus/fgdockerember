FROM ubuntu:18.04

#
# Set time zone
#
ENV TZ=Europe/Budapest
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#
# Install packages
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      software-properties-common \
 && apt-get update \
 && apt-get install -y \
      python \
      python-dev \
      python-tk \
      virtualenv \
      curl \
      wget \
      tar \
      git \
      scons \
      build-essential \
      libssl-dev \
 && curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py \
 && python2 get-pip.py \
 && rm -rf /var/lib/apt/lists/*

#
# Make sure python2 is the highest priority alternative:
#
RUN set -x \
 && update-alternatives \
      --install \
      /usr/bin/python \
      python \
      /usr/bin/python2.7 10

#
# Print python version
#
RUN set -x \
 && python --version

#
# Cmake
#
RUN set -x \
 && cd \
 && pwd \
 && [ -d cmake-3.16.5 ] ||  (wget https://github.com/Kitware/CMake/releases/download/v3.16.5/cmake-3.16.5.tar.gz ; tar zxvf cmake-3.16.5.tar.gz) \
 && rm -rf cmake-3.16.5.tar.gz \
 && cd cmake-3.16.5 \
 && ./bootstrap \
 && make \
 && make install 

#
# Boost
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      libboost-all-dev 


#
# EMBER DEPENDENCIES 
#
#
# Sudials
#
RUN set -x \
 && cd \
 && pwd \
 && [ -d sundials-4.0.2 ] ||  (wget https://github.com/LLNL/sundials/archive/refs/tags/v4.0.2.tar.gz ; mv v4.0.2.tar.gz sundials-4.0.2.tar.gz ; tar zxvf sundials-4.0.2.tar.gz) \
 && rm -rf sundials-4.0.2.tar.gz \
 && mkdir -p sundials-4.0.2/build \
 && cd sundials-4.0.2/build \
 && cmake .. \
 && make \
 && make install

#
# Eigen
# https://gitlab.com/libeigen/eigen.git/ 
#
RUN set -x \
 && cd \
 && pwd \
 && [ -d eigen-3.3.9 ] ||  (wget https://gitlab.com/libeigen/eigen/-/archive/3.3.9/eigen-3.3.9.tar.gz ; tar zxvf eigen-3.3.9.tar.gz) \
 && rm -rf eigen-3.3.9.tar.gz \
 && mkdir -p eigen-3.3.9/build \
 && cd eigen-3.3.9/build \
 && cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local -DINCLUDE_INSTALL_DIR=include \
 && make \
 && make install
 
#
# OneTBB 
#
RUN set -x \
 && cd \
 && pwd \
 && git clone https://github.com/oneapi-src/oneTBB.git 2> /dev/null || (cd oneTBB ; git pull ; cd ..) \
 && cd oneTBB \
 && git checkout tbb_4.4 \
 && make tbb_build_prefix=tbb_local \ 
 && cp /root/oneTBB/build/tbb_local_release/*.so* /usr/local/lib/ \ 
 && cp -r /root/oneTBB/include/tbb/ /usr/local/include/

#
# Cantera
#
RUN set -x \
 && cd \
 && pwd \
 && git clone https://github.com/Cantera/cantera.git 2> /dev/null || (cd cantera ; git pull ; cd ..) \
 && cd cantera \
 && git checkout 2.4 \
 && pip install numpy \ 
 && pip install Cython \
 && pip install ruamel.yaml \
 && scons build \ 
 && scons install
#source /usr/local/bin/setup_cantera

#
# Blas and Lapack
# 
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      libblas-dev \
      liblapack-dev 


#
# EMBER 
#
# in principle, if everything is installed on default paths, these are not necessary:
# && echo "#### Cantera path\ncantera = '/usr/local/'\n" >> ember.conf \
# && echo "#### Sundials path\nsundials = '/usr/local/'\n" >> ember.conf \
# && echo "#### Eigen path\neigen = '/usr/local/'\n" >> ember.conf \
# && echo "#### Boost path\nboost = '/usr/local/'\n" >> ember.conf \
# && echo "#### OneTBB path\ntbb = '/root/oneTBB/build/'\n" >> ember.conf \
# && echo "#### Includes\ninclude = '/usr/local/include/,/root/oneTBB/include/'\n" >> ember.conf \
# && echo "#### Libraries\nlibdirs = '/usr/local/lib/,/root/oneTBB/build/linux_intel64_gcc_cc4.8_libc2.22_kernel4.4.120_release'\n" >> ember.conf \
# && cat ember.conf \
#
#
RUN set -x \
 && cd \
 && pwd \
 && git clone https://github.com/speth/ember.git 2> /dev/null || (cd ember ; git pull ; cd ..) \
 && cd ember \
 && rm -rf ember.conf \
 && touch ember.conf \
 && echo "#### Ember Config file\n\n" >> ember.conf \ 
 && echo "#### Compiler\nCXX = 'g++'\n" >> ember.conf \
 && echo "#### OneTBB path\ntbb = '/root/oneTBB/build/'\n" >> ember.conf \
 && echo "#### Boost path\nboost = '/usr/local/'\n" >> ember.conf \
 && echo "#### Includes\ninclude = '/usr/local/include/,/root/oneTBB/include/'\n" >> ember.conf \
 && echo "#### Libraries\nlibdirs = '/usr/local/lib/,/root/oneTBB/build/tbb_local_release'\n" >> ember.conf \
 && cat ember.conf \ 
 && scons build \
 && scons install install_args=""

#
# Add TBB to path
#
ENV LD_LIBRARY_PATH=/root/oneTBB/build/tbb_local_release/:$LD_LIBRARY_PATH

#
# Add some python packages, so ember works out of the box in this container 
#
RUN set -x \
 && pip install matplotlib \ 
 && pip install h5py 

#
# vim
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      vim 

#
# Clean up unused directories after installtion is complete:
#
RUN set -x \
 && cd \
 && pwd \
 && rm -rf cmake-3.16.5 \
 && rm -rf sundials-4.0.2 \
 && rm -rf eigen-3.3.9 \
 && rm -rf oneTBB \
 && rm -rf cantera 

#
# Test ember
# 
RUN set -x \
 && cd \
 && python ember/python/ember/test/testFlameConfigs.py 

#
# More cleaning
#
RUN set -x \
 && cd \
 && pwd \
 && rm -rf ember
