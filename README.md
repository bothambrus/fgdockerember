# FGDocker

This repository uses the gitlab pipeline to build a Docker image that can run cantera scripts and ember.

It is a copy of [FGDocker](https://gitlab.com/bothambrus/fgdocker) but with modifications to accomodate ember.

Copy from [Best practices for building docker images with GitLab CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/).

## Using this image locally in docker

Getting image:
```
$ sudo docker pull registry.gitlab.com/bothambrus/fgdockerember:latest
```

Displaying locally available images:
```
$ sudo docker image ls
REPOSITORY                                     TAG               IMAGE ID       CREATED          SIZE
registry.gitlab.com/bothambrus/fgdockerember   latest            9c37f7b6a410   23 minutes ago   4.54GB
...
```

Displaying locally available containers:
```
$ sudo docker container ls -a
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS                        PORTS           NAMES
ac1f320e1ba4   86b16dcff452   "/bin/bash"              45 minutes ago   Exited (130) 13 minutes ago                   relaxed_cannon
...
```

Running interactive session, using this docker image:
```
sudo docker run -it registry.gitlab.com/bothambrus/fgdockerember  /bin/bash
```
here `registry.gitlab.com/bothambrus/fgdockerember` refers to the ame of the repository displayed in the first column in the output of `sudo docker image ls`. 
Option `-i` starts an interactive session, i.e.: docker will read the standard input, while option `-t` puts the standard output and error on the terminal. 
The program run in docker is the shell itself: `/bin/bash`.



## Creating singularity image from docker image

Based on this [article](https://www.nas.nasa.gov/hecc/support/kb/converting-docker-images-to-singularity-for-use-on-pleiades_643.html):
```
$ singularity pull fg_ember.sif docker://registry.gitlab.com/bothambrus/fgdockerember:latest
```

